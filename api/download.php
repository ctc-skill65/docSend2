<?php
require_once __DIR__ . '/../boot.php';

$error = '<h1>Not Found</h1>';

$id = get('id');
if (empty($id)) {
    exit($error);
}

$doc = db_row("SELECT * FROM `documents` WHERE `doc_id`='{$id}'");
if (empty($doc)) {
    exit($error);
}

$db->query("UPDATE `documents` SET `download`=`download`+1 WHERE `doc_id`='{$id}';");
redirect($doc['doc_file']);

echo $error;
