<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$id = get('id');
$list_path = '/admin/doc-types/list.php';

if (!isset($id)) {
    redirect($list_path);
}

$page_path = "/admin/doc-types/edit.php?id={$id}";

if (post()) {
    $qr = $db->query("UPDATE `document_types` SET `doc_type_name`='{$_POST['doc_type']}' WHERE `doc_type_id`='{$id}'");

    if ($qr) {
        setAlert('success', "แก้ไขประเภทเอกสาร {$_POST['doc_type']} สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขประเภทเอกสาร {$_POST['doc_type']} ได้");
    }

    redirect($list_path);
}

$data = db_row("SELECT * FROM `document_types` WHERE `doc_type_id`='{$id}'");

ob_start();
?>
<?= showAlert() ?>

<form method="post">
    <label for="doc_type">ชื่อประเภทเอกสาร</label>
    <input type="text" name="doc_type" id="doc_type" value="<?= $data['doc_type_name'] ?>" required>
    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขประเภทเอกสาร';

require ROOT . '/admin/layout.php';
