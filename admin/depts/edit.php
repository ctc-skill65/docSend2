<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$id = get('id');
$list_path = '/admin/depts/list.php';

if (!isset($id)) {
    redirect($list_path);
}

$page_path = "/admin/depts/edit.php?id={$id}";

if (post()) {
    $qr = $db->query("UPDATE `departments` SET `dept_name`='{$_POST['dept']}' WHERE `dept_id`='{$id}'");

    if ($qr) {
        setAlert('success', "แก้ไขแผนกหรืองานต่างๆ {$_POST['dept']} สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขแผนกหรืองานต่างๆ {$_POST['dept']} ได้");
    }

    redirect($list_path);
}

$data = db_row("SELECT * FROM `departments` WHERE `dept_id`='{$id}'");

ob_start();
?>
<?= showAlert() ?>

<form method="post">
    <label for="dept">ชื่อแผนกหรืองานต่างๆ</label>
    <input type="text" name="dept" id="dept" value="<?= $data['dept_name'] ?>" required>
    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขแผนกหรืองานต่างๆ';

require ROOT . '/admin/layout.php';
