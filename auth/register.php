<?php
require_once __DIR__ . '/../boot.php';

$page_path = '/auth/register.php';

if (post()) {

    $check = db_result("SELECT * FROM `users` WHERE `email`='{$_POST['email']}'");
    if (!empty($check)) {
        setAlert('error', "มีอีเมล {$_POST['email']} แล้วไม่สามารถสมัครซ้ำได้");
        redirect($page_path);
    }

    $hash = md5(post('password'));
    $qr = $db->query("INSERT INTO `users`(
    `firstname`, 
    `lastname`, 
    `email`, 
    `password`, 
    `dept_id`, 
    `user_type`, 
    `status`) VALUES (
    '{$_POST['firstname']}',
    '{$_POST['lastname']}',
    '{$_POST['email']}',
    '{$hash}',
    '{$_POST['dept']}',
    'user',
    0)");

    if ($qr) {
        setAlert('success', 'สมัครสมาชิกสำเร็จเรียบร้อย บัญชีอยู่ระหว่างขออนุญาตใช้งานระบบ');
    } else {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถสมัครสมาชิกได้');
    }

    redirect($page_path);
}

$items = db_result("SELECT * FROM `departments`");

ob_start();
?>
<h1>สมัครสมาชิก</h1>
<h2>ระบบส่งเอกสารออนไลน์</h2>

<?= showAlert() ?>
<form method="post">
    <label for="firstname">ชื่อ</label>
    <input type="text" name="firstname" id="firstname" required>
    <br>
    <label for="lastname">นามสกุล</label>
    <input type="text" name="lastname" id="lastname" required>
    <br>
    <label for="email">อีเมล</label>
    <input type="email" name="email" id="email" required>
    <br>
    <label for="password">รหัสผ่าน</label>
    <input type="password" name="password" id="password" required>
    <br> 
    <label for="dept">แผนกหรืองานต่างๆ</label>
    <select name="dept" id="dept">
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($items as $item) : ?>
            <option value="<?= $item['dept_id'] ?>"><?= $item['dept_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>
    <button type="submit">สมัครสมาชิก</button>
</form>

<p>
    คุณมีบัญชีแล้ว? <a href="<?= url('/auth/login.php') ?>">เข้าสู่ระบบ</a>  
</p>
<?php
$layout_body = ob_get_clean();
$page_name = 'สมัครสมาชิก';

require INC . '/base_layout.php';
