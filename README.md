# docSend2 10/1/66

# Config file
./config.php

example
```php
<?php

return [
    'site_url' => 'http://skill65.local/docSend2 ',
    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_docSend2 ',
    'db_port' => 3306,
    'db_charset' => 'utf8mb4'
];

```
# Database

./skill65_docSend2 .sql

admin 
email: admin@demo.com 
password: 12345678 

user 
email: user@demo.com
password: 12345678

