<?php

return [
    'site_url' => 'http://skill65.local/docSend2',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_docSend2',
    'db_charset' => 'utf8mb4'
];
