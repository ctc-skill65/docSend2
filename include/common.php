<?php

function conf($key) {
    global $config;

    return isset($config[$key]) ? $config[$key] : null;
}

function url($path = '') {
    return conf('site_url') . $path;
}

function redirect($path = '') {
    header('Location: ' . url($path));
    exit;
}

function get($key = null) {
    if ($key === null) {
        return $_GET;
    }

    return isset($_GET[$key]) ? $_GET[$key] : null;
}

function post($key = null) {
    if ($key === null) {
        return $_POST;
    }

    return isset($_POST[$key]) ? $_POST[$key] : null;
}

function upload($key, $dir = '/storage') {
    if (empty($_FILES[$key]['name']) || empty($_FILES[$key]['tmp_name'])) {
        return false;
    }

    $path = $dir . '/';
    $path .= uniqid();
    $path .= '.' . pathinfo($_FILES[$key]['name'], PATHINFO_EXTENSION);

    $full_path = ROOT . $path;

    move_uploaded_file($_FILES[$key]['tmp_name'], $full_path);
    if (is_file($full_path)) {
        return $path;
    }

    return false;
}

function setFlash($key, $value) {
    $_SESSION['flash'][$key] = $value;
}

function getFlash($key, $unset = false) {
    global $flash;

    if (isset($_SESSION['flash'][$key])) {
        $value = $_SESSION['flash'][$key];
        if ($unset) {
            unset($_SESSION['flash'][$key]);
        }
        return $value;
    }

    return isset($flash[$key]) ? $flash[$key] : null; 
}

function setAlert($status, $message) {
    setFlash('alert', [
        'status' => $status,
        'message' => $message
    ]);
}

function showAlert() {
    $alert = getFlash('alert');

    if ($alert === null) {
        return;
    }

    $status = $alert['status'];
    $message = $alert['message'];
    ?>
    <script>
        window.onload = function () {
            setTimeout(() => {
                alert('<?= htmlspecialchars($message) ?>');
            }, 50);
        }
    </script>
    <?php
}

function clickConfirm($message) {
    echo "onclick=\"return confirm('" . htmlspecialchars($message) . "')\"";
}

function db_result($sql) {
    global $db;

    $result = $db->query($sql);

    $data = [];
    while ($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    return $data;
}

function db_row($sql) {
    global $db;

    $result = $db->query($sql);
    return $result->fetch_assoc();
}

function checkLogin() {
    global $user, $user_id;

    $login_page = '/auth/login.php';
    if (empty($_SESSION['user_id'])) {
        redirect($login_page);
    }

    $user_id = $_SESSION['user_id'];

    $user = db_row("SELECT * FROM `users` WHERE `user_id`='{$user_id}'");
    
    if (empty($user) || $user['status'] !== '1') {
        redirect($login_page);
    }
}

function checkAuth($user_type) {
    global $user;

    checkLogin();

    if (empty($user) || $user['user_type'] !== $user_type) {
        redirect('/auth/login.php');
    }
}

function getUser($id) {
    return db_row("SELECT * FROM `users` WHERE `user_id`='{$id}'");
}

function printUserData($id) {
    $user = getUser($id);

    $data = isset($user['firstname']) ? $user['firstname'] : null;
    $data .= ' ';
    $data .= isset($user['lastname']) ? $user['lastname'] : null;
    $data .= ' (';
    $data .= isset($user['email']) ? $user['email'] : null;
    $data .= ')';

    return $data;
}
