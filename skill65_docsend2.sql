-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2023 at 03:22 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skill65_docsend2`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`dept_id`, `dept_name`) VALUES
(1, 'ฝ่ายการตลาด'),
(2, 'ฝ่ายขาย'),
(3, 'ฝ่ายบุคคล'),
(4, 'ฝ่ายลูกค้าสัมพันธ์'),
(5, 'ฝ่ายบัญชี/การเงิน');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `doc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `doc_name` varchar(60) NOT NULL,
  `doc_date` date NOT NULL,
  `doc_type_id` int(11) NOT NULL,
  `doc_file` varchar(60) NOT NULL,
  `download` int(11) DEFAULT 0,
  `send_type` enum('user','dept') NOT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `to_dept_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`doc_id`, `user_id`, `doc_name`, `doc_date`, `doc_type_id`, `doc_file`, `download`, `send_type`, `to_user_id`, `to_dept_id`, `created_at`) VALUES
(1, 3, 'รายงาน', '2023-01-10', 1, '/storage/docs/63bd508cb8422.pdf', 2, 'user', 6, NULL, '2023-01-10 18:48:28'),
(2, 3, 'รายงาน2', '2023-01-10', 2, '/storage/docs/63bd51144ac4a.pdf', 0, 'user', 7, NULL, '2023-01-10 18:50:44'),
(3, 6, 'รายงาน3', '2023-01-10', 4, '/storage/docs/63bd519fd7ba5.pdf', 0, 'user', 3, NULL, '2023-01-10 18:53:03'),
(4, 6, 'รายงาน101', '2023-01-10', 3, '/storage/docs/63bd52790e4f6.pdf', 2, 'dept', NULL, 2, '2023-01-10 18:56:41');

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

DROP TABLE IF EXISTS `document_types`;
CREATE TABLE `document_types` (
  `doc_type_id` int(11) NOT NULL,
  `doc_type_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`doc_type_id`, `doc_type_name`) VALUES
(1, 'เอกสารทั่วไป'),
(2, 'เอกสารฉบับร่าง'),
(3, 'เอกสารต้นฉบับ'),
(4, 'เอกสารสำเนา');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `user_type` enum('admin','user') NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `email`, `password`, `dept_id`, `user_type`, `status`) VALUES
(2, 'admin', 'demo', 'admin@demo.com', '25d55ad283aa400af464c76d713c07ad', 0, 'admin', 1),
(3, 'user', 'demo', 'user@demo.com', '25d55ad283aa400af464c76d713c07ad', 2, 'user', 1),
(4, 'user2', 'demo', 'user2@demo.com', '25d55ad283aa400af464c76d713c07ad', 0, 'user', -1),
(6, 'user3', 'demo', 'user3@demo.com', '25d55ad283aa400af464c76d713c07ad', 1, 'user', 1),
(7, 'user4', 'demo', 'user4@demo.com', '25d55ad283aa400af464c76d713c07ad', 1, 'user', 1),
(8, 'user5', 'demo', 'user5@demo.com', '25d55ad283aa400af464c76d713c07ad', 2, 'user', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`doc_id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`doc_type_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `doc_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
