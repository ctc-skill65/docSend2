<?php

define('ROOT', __DIR__);
define('INC', __DIR__ . '/include');

session_start();
$flash;
if (isset($_SESSION['flash'])) {
    $flash = $_SESSION['flash'];
    unset($_SESSION['flash']);
}

$config = require ROOT . '/config.php';

require_once INC . '/common.php';
require_once INC . '/database.php';
