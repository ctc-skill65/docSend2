<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = '/user/send/user.php';

if (post()) {
    $file = upload('file', '/storage/docs');
    if (!$file) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดไฟล์เอกสารได้");
        redirect($page_path);
    }

    $qr = $db->query("INSERT INTO `documents`( 
    `user_id`, 
    `doc_name`, 
    `doc_date`, 
    `doc_type_id`, 
    `doc_file`,  
    `send_type`, 
    `to_dept_id`, 
    `created_at`) 
    VALUES (
    '{$user_id}',
    '{$_POST['doc_name']}',
    '{$_POST['doc_date']}',
    '{$_POST['doc_type_id']}',
    '{$file}',
    'dept',
    '{$_POST['to_dept']}',
    now())");

    if ($qr) {
        setAlert('success', "ส่งเอกสาร {$_POST['doc_name']} สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถส่งเอกสาร {$_POST['doc_name']} ได้");
    }

    redirect($page_path);
}

$doc_types = db_result("SELECT * FROM `document_types`");
$items = db_result("SELECT * FROM `departments`");

ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="doc_name">ชื่อเอกสาร</label>
    <input type="text" name="doc_name" id="doc_name" required>
    <br>

    <label for="doc_date">วันที่พิมพ์เอกสาร</label>
    <input type="date" name="doc_date" id="doc_date" required>
    <br>

    <label for="doc_type_id">ประเภทเอกสาร</label>
    <select name="doc_type_id" id="doc_type_id">
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($doc_types as $item) : ?>
            <option value="<?= $item['doc_type_id'] ?>"><?= $item['doc_type_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <label for="file">ไฟล์เอกสาร</label>
    <input type="file" name="file" id="file" required>
    <br>

    <label for="to_dept">ส่งถึง</label>
    <select name="to_dept" id="to_dept" required>
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($items as $item) : ?>
            <option value="<?= $item['dept_id'] ?>"><?= $item['dept_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'ส่งเอกสารให้แผนกหรืองานต่างๆ';

require ROOT . '/user/layout.php';
