<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = '/user/docs/dept-list.php';

$items = db_result("SELECT * FROM `documents`
LEFT JOIN `document_types` ON `document_types`.`doc_type_id`=`documents`.`doc_type_id`
LEFT JOIN `departments` ON `departments`.`dept_id`=`documents`.`to_dept_id`
WHERE `documents`.`to_dept_id`='{$user['dept_id']}'");

ob_start();
?>
<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ชื่อเอกสาร</th>
            <th>วันที่พิมพ์เอกสาร</th>
            <th>ประเภทเอกสาร</th>
            <th>ประเภทการส่ง</th>
            <th>ส่งจาก</th>
            <th>ส่งถึง</th>
            <th>จำนวนดาวน์โหลด</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['doc_id'] ?></td>
                <td><?= $item['doc_name'] ?></td>
                <td><?= $item['doc_date'] ?></td>
                <td><?= $item['doc_type_name'] ?></td>
                <td>
                    <?php
                    switch ($item['send_type']) {
                        case 'user':
                            echo 'ส่งถึงผู้ใช้งาน';
                            break;

                        case 'dept':
                            echo 'ส่งถึงแผนกหรืองานต่างๆ';
                            break;
                    }
                    ?>
                </td>
                <td><?= printUserData($item['user_id']) ?></td>
                <td>
                    <?php
                    switch ($item['send_type']) {
                        case 'user':
                            echo printUserData($item['to_user_id']);
                            break;

                        case 'dept':
                            echo 'แผนก: ' . $item['dept_name'];
                            break;
                    }
                    ?>
                </td>
                <td><?= $item['download'] ?></td>
                <td>
                    <a href="<?= url('/api/download.php?id=' . $item['doc_id']) ?>" target="_blank" rel="noopener noreferrer">
                        <button>
                            ดาวน์โหลด
                        </button>
                    </a>
                 </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการเอกสารแผนกหรืองานต่างๆที่ผู้ใช้งานอยู่';

require ROOT . '/user/layout.php';
